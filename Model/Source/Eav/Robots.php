<?php
/**
 * @category  Beside
 * @package   Beside_Catalog
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Catalog\Model\Source\Eav;

/**
 * Class Robots
 * @package Beside\Catalog\Model\Source\Eav\Source
 */
class Robots extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Retrieve all options for the source from configuration
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    public function getAllOptions()
    {
        return [
            ['value' => 'default', 'label' => 'Default'],
            ['value' => 'index,follow', 'label' => 'index,follow'],
            ['value' => 'index,nofollow', 'label' => 'index,nofollow'],
            ['value' => 'noindex,follow', 'label' => 'noindex,follow'],
            ['value' => 'noindex,nofollow', 'label' => 'noindex,nofollow']
        ];
    }
}
