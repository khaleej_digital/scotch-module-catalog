<?php
declare(strict_types=1);

namespace Beside\Catalog\Model\Import;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Zend_Validate_Exception;
use Beside\Catalog\Model\Import\Logger\Logger;

/**
 * Class ProductAttribute
 *
 * @package Beside\Catalog\Model\Import
 */
class ProductAttribute
{
    /**
     * @var string
     */
    private const CUSTOM_ATTRIBUTE_GROUP_NAME = 'Custom attributes';

    /**
     * @var ProductReader
     */
    private ProductReader $productReader;

    /**
     * @var EavSetupFactory
     */
    private EavSetupFactory $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private ModuleDataSetupInterface $moduleDataSetup;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * ProductAttribute constructor.
     *
     * @param ProductReader $productReader
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param Logger $logger
     */
    public function __construct(
        ProductReader $productReader,
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup,
        Logger $logger
    ) {
        $this->productReader = $productReader;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->logger = $logger;
    }

    /**
     * Import product attributes
     *
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function importProductAttributes()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $productAttributes = $this->productReader->getListAttributes();

        foreach ($productAttributes as $productAttributeCode) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $productAttributeCode,
                [
                    'type' => 'text',
                    'group' => self::CUSTOM_ATTRIBUTE_GROUP_NAME,
                    'backend' => '',
                    'frontend' => '',
                    'label' => $productAttributeCode,
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => true,
                    'filterable' => true,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
    }
}
