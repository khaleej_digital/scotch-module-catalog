<?php
namespace Beside\Catalog\Model\Import\Logger;

use Monolog\Logger;

/**
 * Class Handler
 *
 * @package Beside\Catalog\Model\Import\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     *
     * @var string
     */
    protected $fileName = '/var/log/import.log';
}
