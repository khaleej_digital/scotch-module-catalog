<?php
declare(strict_types=1);

namespace Beside\Catalog\Model\Import;

use Exception;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Beside\Catalog\Model\Import\Logger\Logger;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Framework\App\State;

/**
 * Class Category
 *
 * @package Beside\Catalog\Model\Import
 */
class Category
{
    /**
     * @var string
     */
    const ADMIN_STORE_ID = '0';

    /**
     * @var string
     */
    const ADMIN_STORE_NAME = 'adminhtml';

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;

    /**
     * @var CategoryReader
     */
    private CategoryReader $categoryReader;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Emulation
     */
    private Emulation $emulation;

    /**
     * @var State
     */
    private State $state;

    /**
     * Category constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryFactory $categoryFactory
     * @param CategoryReader $categoryReader
     * @param StoreManagerInterface $storeManager
     * @param Emulation $emulation
     * @param State $state
     * @param Logger $logger
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        CategoryFactory $categoryFactory,
        CategoryReader $categoryReader,
        StoreManagerInterface $storeManager,
        Emulation $emulation,
        State $state,
        Logger $logger
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->categoryFactory = $categoryFactory;
        $this->categoryReader = $categoryReader;
        $this->storeManager = $storeManager;
        $this->emulation = $emulation;
        $this->state = $state;
        $this->logger = $logger;
    }

    /**
     * Import categories
     *
     * @return void
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function importCategories(): void
    {
        try {
            $this->state->getAreaCode();
        } catch (Exception $e) {
            $this->state->setAreaCode(Area::AREA_ADMINHTML);
        }

        $importCategories = $this->categoryReader->getCategories();
        $this->iterateCategories($importCategories);
        $importSubCategories = $this->categoryReader->getSubCategories();
        $this->iterateCategories($importSubCategories);
    }

    /**
     * Iterate categories
     *
     * @param array $importCategories
     *
     * @return void
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    private function iterateCategories(array $importCategories): void
    {
        $this->emulation->startEnvironmentEmulation(self::ADMIN_STORE_ID, self::ADMIN_STORE_NAME);
        foreach ($importCategories as $importCategory) {
            $this->saveCategory($importCategory);
        }
        $this->emulation->stopEnvironmentEmulation();
    }

    /**
     * Save category
     *
     * @param array $importCategory
     *
     * @return void
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    private function saveCategory(array $importCategory): void
    {
        if (!$importCategory['id']) {

            $category = $this->categoryFactory->create();
        } else {
            $category = $this->categoryRepository->get($importCategory['id']);
        }

        $category->setUrlKey($importCategory['url_key'])
            ->setName($importCategory['name'])
            ->setParentId($importCategory['parent_id'])
            ->setIsActive(true);

        $category->setDescription($importCategory['description'] ?? '');
        $category->setImage($importCategory['image'] ?? '');


        try {
            $this->categoryRepository->save($category);
        } catch (Exception $e) {
            $this->logger->critical(__('Can not save category with url_key ' . $importCategory['url_key'] . ' : %1', $e->getMessage()));
        }
    }
}
