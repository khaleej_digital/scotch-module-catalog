<?php
declare(strict_types=1);

namespace Beside\Catalog\Model\Import;

use Magento\Eav\Model\Config;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ProductAttribute
 *
 * @package Beside\Catalog\Model\Import
 */
class ProductReader
{
    /**
     * @var string
     */
    private const DIR_IMPORT_PRODUCT_ATTRIBUTE = 'product';

    /**
     * @var ReaderXml
     */
    private ReaderXml $readerXml;

    /**
     * @var Config
     */
    private Config $eavConfig;

    /**
     * @var EavSetup
     */
    private EavSetup $eavSetup;

    /**
     * ProductAttribute constructor.
     *
     * @param ReaderXml $readerXml
     * @param Config $eavConfig
     * @param EavSetup $eavSetup
     */
    public function __construct(
        ReaderXml $readerXml,
        Config $eavConfig,
        EavSetup $eavSetup
    ) {
        $this->readerXml = $readerXml;
        $this->eavConfig = $eavConfig;
        $this->eavSetup = $eavSetup;
    }

    /**
     * Get import data
     *
     * @return array
     * @throws FileSystemException
     */
    private function getImportData(): array
    {
        return $this->readerXml->getContent(self::DIR_IMPORT_PRODUCT_ATTRIBUTE);
    }

    /**
     * Get list of new attributes
     *
     * @return array
     * @throws FileSystemException
     * @throws LocalizedException
     */
    public function getListAttributes(): array
    {
        $result = [];
        $data = $this->getImportData();
        $productData = $data['catalog']['_value']['product'] ?? [];
        if (!empty($productData)) {
            foreach ($productData as $product) {
                foreach ($product['_value']['custom-attributes']['custom-attribute'] as $attribute) {
                    if (!empty($attribute['_attribute']['attribute-id'])) {
                        $attributeCode = $this->validateAttribute($attribute['_attribute']['attribute-id']);
                        if (!in_array($attributeCode, $result) &&
                            !$this->isProductAttributeExists($attributeCode)) {
                            $result[] = $attributeCode;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * validate attribute
     *
     * @param $attributeCode
     *
     * @return string|string[]
     */
    private function validateAttribute($attributeCode)
    {
         return str_replace('-', '_', $attributeCode);
    }

    /**
     * Is product attribute exists
     *
     * @param $codeAttribute
     *
     * @return bool
     * @throws LocalizedException
     */
    private function isProductAttributeExists($codeAttribute): bool
    {
        $attribute = $this->eavConfig->getAttribute(ModelProduct::ENTITY, $codeAttribute);

        return $attribute && $attribute->getId();
    }
}
