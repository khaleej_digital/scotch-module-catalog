<?php

namespace Beside\Catalog\Model\Import;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DirectoryList as Dir;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Xml\Parser;
use Magento\Framework\Filesystem\Io\File;
use Beside\Catalog\Model\Import\Logger\Logger;

/**
 * Class ReaderXml
 *
 * @package Beside\Catalog\Model\Import
 */
class ReaderXml
{
    /**
     * @var string
     */
    private const ROOT_DIR = '/import/';

    /**
     * @var Parser
     */
    private Parser $parserXml;

    /**
     * @var Dir
     */
    private Dir $dir;

    /**
     * @var File
     */
    private File $file;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * ReadXml constructor.
     *
     * @param Parser $parserXml
     * @param Dir $dir
     * @param File $file
     * @param Logger $logger
     */
    public function __construct(
        Parser $parserXml,
        Dir $dir,
        File $file,
        Logger $logger
    ) {
        $this->parserXml = $parserXml;
        $this->dir = $dir;
        $this->file = $file;
        $this->logger = $logger;
    }

    /**
     * Get content of XML file
     *
     * @param string $targetFolder
     * @return array
     * @throws FileSystemException
     */
    public function getContent($targetFolder): array
    {
        $result = [];
        $filePath = '';

        if(!file_exists($this->getImportDir($targetFolder))){
            @mkdir($this->getImportDir($targetFolder), 0777, true);
        }

        $fileDir = $this->getImportDir($targetFolder);
        $filePath = $this->getImportFile($fileDir);

        if ($filePath) {
            $result = $this->parserXml->load($filePath)->xmlToArray();
        }

        return $result;
    }

    /**
     * Get import dir
     *
     * @param $targetFolder
     * @return string
     * @throws FileSystemException
     */
    protected function getImportDir($targetFolder): string
    {
        return $this->dir->getPath(DirectoryList::VAR_DIR) . self::ROOT_DIR . $targetFolder;
    }

    /**
     * Get import files
     *
     * @param $fileDir
     * @return mixed
     */
    private function getImportFile($fileDir)
    {
        $files = [];
        $iterator = new \DirectoryIterator($fileDir);

        foreach($iterator as $file){
            if ($file->isDot()
                || $file->isDir()
                || !preg_match('/^.*\.xml$/i',$file->getFilename())
            ) continue;

            $files[] = $file->getPathname();
        }
        sort($files);

        $result = current($files);

        if (FALSE === $result) {
            $this->logger->critical(__('Dir for import is empty'));
        }

        return $result;
    }
}
