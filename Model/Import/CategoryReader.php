<?php
declare(strict_types=1);

namespace Beside\Catalog\Model\Import;

use Magento\Framework\Exception\FileSystemException;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CategoryReader
 *
 * @package Beside\Catalog\Model\Import
 */
class CategoryReader
{
    /**
     * @var string
     */
    private const DIR_IMPORT_CATEGORY = 'category';

    /**
     * @var string
     */
    private const IMPORT_LANG = 'en';

    /**
     * Mapping of attributes of XML and Magento
     *
     * @var array
     */
    private const MAP_NAME_ATTRIBUTE = [
        'url_key' => 'url_key',
        'categoryTitle' => 'name',
        'categoryDescription' => 'description',
        'categoryImage' => 'image'
    ];

    /**
     * @var array
     */
    private const REQUIRED_ATTRIBUTE = ['url_key', 'name'];

    /**
     * @var ReaderXml
     */
    private ReaderXml $readerXml;

    /**
     * @var array
     */
    private array $topLevelCategories = [];

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionCategoryFactory;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * Category constructor.
     *
     * @param ReaderXml $readerXml
     * @param CollectionFactory $collectionCategoryFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ReaderXml $readerXml,
        CollectionFactory $collectionCategoryFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->readerXml = $readerXml;
        $this->collectionCategoryFactory = $collectionCategoryFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Get Import category data
     *
     * @return array
     * @throws FileSystemException
     */
    private function getImportData(): array
    {
        return $this->readerXml->getContent(self::DIR_IMPORT_CATEGORY);
    }

    /**
     * Get categories
     *
     * @return array
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCategories(): array
    {
        if (count($this->topLevelCategories) > 0) {
            return $this->topLevelCategories;
        }
        $this->topLevelCategories = [];
        $categories = [];
        $importData = $this->getImportData();

        $categoriesImportData = $importData['catalog']['_value']['category'] ?? [];
        if (!empty($categoriesImportData)) {
            foreach ($categoriesImportData as $category) {
                $urlKey = $category['_attribute']['category-id'];
                $newCategory = [
                    'url_key' => $urlKey,
                    'parent_id' => $this->storeManager->getDefaultStoreView()->getRootCategoryId(),
                    'id' => $this->getCategoryIdByUrlKey($urlKey)
                ];

                foreach ($category['_value']['custom-attributes']['custom-attribute'] as $attribute) {
                    $attributeName = self::MAP_NAME_ATTRIBUTE[$attribute['_attribute']['attribute-id']];

                    if (!empty($attributeName)) {
                        if (empty($attribute['_attribute']['lang'])) {
                            $newCategory[$attributeName] = $this->validateAttributeValue($attributeName, $attribute['_value']);
                        } else {
                            if ($attribute['_attribute']['lang'] === self::IMPORT_LANG) {
                                $newCategory[$attributeName] = $this->validateAttributeValue($attributeName, $attribute['_value']);
                            }
                        }
                    }
                }

                if (isset($category['_value']['display-name'])) {
                    foreach ($category['_value']['display-name'] as $name) {
                        if ($name['_attribute']['lang'] == self::IMPORT_LANG) {
                            $newCategory['name'] = $name['_value'];
                        }
                    }
                }

                if (TRUE === $this->validateCategory($newCategory)) {
                    $categories[$newCategory['url_key']] = $newCategory;
                }
            }

            $this->topLevelCategories = $categories;
        }

        return $this->topLevelCategories;
    }

    /**
     * Get sub categories
     *
     * @return array
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getSubCategories(): array
    {
        $subCategories = [];
        $importData = $this->getImportData();
        $categories = $this->getCategories();

        $categoriesAssigment = $importData['catalog']['_value']['category-assignment'] ?? [];
        if (!empty($categoriesAssigment)) {
            foreach ($categoriesAssigment as $subCategory) {
                $subCategoryUrlKey = $subCategory['_attribute']['category-id'];
                $pos = strripos($subCategoryUrlKey, '-');

                if (FALSE !== $pos) {
                    $subCategoryName = substr($subCategoryUrlKey, $pos + 1);
                    $parentCategoryUrlKey = substr($subCategoryUrlKey, 0, $pos);

                    if (array_key_exists($parentCategoryUrlKey, $categories) &&
                        !isset($subCategories[$subCategoryUrlKey])) {
                        $parentCategoryId = $this->getCategoryIdByUrlKey($parentCategoryUrlKey);

                        if ($parentCategoryId) {
                            $subCategories[$subCategoryUrlKey] = [
                                'url_key' => $subCategoryUrlKey,
                                'name' => ucwords(strtolower($subCategoryName)),
                                'parent_url_key' => $parentCategoryUrlKey,
                                'parent_id' => $parentCategoryId,
                                'id' => $this->getCategoryIdByUrlKey($subCategoryUrlKey),
                                'description' => '',
                                'image' => ''
                            ];
                        }
                    }
                }
            }
        }

        return $subCategories;
    }

    /**
     * Get category id by url_key
     *
     * @param $urlKey
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getCategoryIdByUrlKey(string $urlKey): string
    {
        $collection = $this->collectionCategoryFactory->create()
            ->addAttributeToSelect('entity_id')
            ->setStore($this->storeManager->getStore())
            ->addAttributeToFilter('url_key', $urlKey);

        return $collection->getFirstItem()->getEntityId() ?? '';
    }

    /**
     * Validate attribute value
     *
     * @param string $code
     * @param $value
     * @return string
     */
    private function validateAttributeValue($code, $value): string
    {
        $result = $value;

        switch ($code) {
            case 'name':
                $result = ucwords(strtolower($value));
                break;
            default:
                $result = $value;
        }

        return $result;
    }

    /**
     * Validate category
     *
     * @param $category
     *
     * @return bool
     */
    private function validateCategory($category): bool
    {
        $result = TRUE;
        $codesAttribute = array_keys($category);

        foreach (self::REQUIRED_ATTRIBUTE as $attributeCode) {
            if (!in_array($attributeCode, $codesAttribute)) {
                $result = FALSE;
            }
        }

        return $result;
    }
}
