<?php
declare(strict_types=1);

namespace Beside\Catalog\Model;

use Exception;
use Magento\Cron\Model\Config\Source\Frequency;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ValueFactory;
use Psr\Log\LoggerInterface;

/**
 * Class CronMessageBuilder
 *
 * @package Beside\Catalog\Model
 */
class CronBuilder
{
    /**
     * @var ValueFactory
     */
    private ValueFactory $valueFactory;


    /**
     * @var string
     */
    private string $runModelPath = '';

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * CronBuilder constructor.
     *
     * @param ValueFactory $valueFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ValueFactory $valueFactory,
        LoggerInterface $logger
    ) {
        $this->valueFactory = $valueFactory;
        $this->logger = $logger;
    }

    /**
     * Get cron expresssion as a string
     *
     * @param string $frequency
     * @param array $time
     *
     * @return string
     */
    public function getCronExpressionString(string $frequency, array $time): string
    {
        $cronExprArray = [
            (int)$time[1], //Minute
            (int)$time[0], //Hour
            $frequency == Frequency::CRON_MONTHLY ? '1' : '*', //Day of the Month
            '*', //Month of the Year
            $frequency == Frequency::CRON_WEEKLY ? '1' : '*', //Day of the Week
        ];

        $cronExprString = join(' ', $cronExprArray);

        return $cronExprString;
    }

    /**
     * Set cron config value
     *
     * @return void
     * @throws Exception
     */
    public function setCron(string $cronExprString, string $path): void
    {
        try {
            $cronPathConfig = $this->configValueFactory->create();
            $cronPathConfig = $cronPathConfig->load($path,'path');
            $cronPathConfig->setValue($cronExprString);
            $cronPathConfig->setPath($path);
            $cronPathConfig->save();
        } catch (Exception $e) {
            throw new Exception(__('We can\'t save the cron expression.'));
        }
    }
}
