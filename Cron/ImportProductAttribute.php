<?php
declare(strict_types=1);

namespace Beside\Catalog\Cron;

use Beside\Catalog\Model\Import\ProductAttribute;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ImportProductAttribute
 *
 * @package Beside\Catalog\Cron
 */
class ImportProductAttribute
{
    /**
     * @var ProductAttribute
     */
    private ProductAttribute $productAttribute;

    /**
     * ImportProductAttribute constructor.
     *
     * @param ProductAttribute $productAttribute
     */
    public function __construct(
        ProductAttribute $productAttribute
    ) {
        $this->productAttribute = $productAttribute;
    }

    /**
     * Import
     *
     * @return void
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function execute(): void
    {
        $this->productAttribute->importProductAttributes();
    }
}
