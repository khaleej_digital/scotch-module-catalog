<?php
declare(strict_types=1);

namespace Beside\Catalog\Cron\Model\Config\Backend;

use Exception;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\App\Config\ValueInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Beside\Catalog\Model\CronBuilder;
use Psr\Log\LoggerInterface;

/**
 * Class ImportProductAttribute
 *
 * @package Beside\Catalog\Cron\Model\Config\Backend
 */
class ImportProductAttribute extends Value
{
    /**
     * @var string
     */
    const CRON_STRING_PATH = 'crontab/beside_import/jobs/beside_catalog_update_product_attributes/schedule/cron_expr';

    /**
     * @var string
     */
    const CRON_MODEL_PATH = 'crontab/beside_import/jobs/beside_catalog_update_product_attributes/run/model';

    /** @var string XML path to import attributes time settings */
    const XML_PATH_IMPORT_ATTRIBUTES_TIME_VALUE = 'groups/beside_import_product_attributes_cron/fields/time/value';

    /** @var string XML path to import attributes frequency settings */
    const XML_PATH_IMPORT_ATTRIBUTES_FREQUENCY_VALUE = 'groups/beside_import_product_attributes_cron/fields/frequency/value';

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var CronBuilder
     */
    private CronBuilder $cronBuilder;

    /**
     * ImportProductAttribute constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param LoggerInterface $logger
     * @param CronBuilder $cronBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        LoggerInterface $logger,
        CronBuilder $cronBuilder,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
        $this->logger = $logger;
        $this->cronBuilder = $cronBuilder;
    }

    /**
     * After save config
     *
     * @return ValueInterface
     */
    public function afterSave(): ValueInterface
    {
        $time = $this->getData(self::XML_PATH_IMPORT_ATTRIBUTES_TIME_VALUE);
        $frequency = $this->getData(self::XML_PATH_IMPORT_ATTRIBUTES_FREQUENCY_VALUE);

        $cronExprString = $this->cronBuilder->getCronExpressionString($frequency, $time);
        try {
            $this->cronBuilder->setCron($cronExprString, self::CRON_STRING_PATH);
            $this->cronBuilder->setCron($cronExprString, self::CRON_MODEL_PATH);
        } catch (Exception $e) {
            $this->logger->error('We can\'t save the cron expression.' . $e->getMessage());
        }

        return parent::afterSave();
    }
}
