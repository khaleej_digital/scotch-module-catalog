<?php
declare(strict_types=1);

namespace Beside\Catalog\Cron;

use Beside\Catalog\Model\Import\Category as ModelImportCategory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ImportCategory
 *
 * @package Beside\Catalog\Cron
 */
class ImportCategory
{
    /**
     * @var ModelImportCategory
     */
    private ModelImportCategory $importCategory;

    /**
     * ImportCategory constructor.
     *
     * @param ModelImportCategory $importCategory
     */
    public function __construct(
        ModelImportCategory $importCategory
    ) {
        $this->importCategory = $importCategory;
    }

    /**
     * Import
     *
     * @return void
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(): void
    {
        $this->importCategory->importCategories();
    }
}
