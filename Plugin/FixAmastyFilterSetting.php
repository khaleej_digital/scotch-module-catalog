<?php
declare(strict_types=1);

namespace Beside\Catalog\Plugin;

use Magento\Framework\Registry;
use Amasty\ShopbyBase\Model\FilterSetting;

/**
 * Class FixAmastyFilterSetting
 *
 * @package Beside\Catalog\Plugin
 */
class FixAmastyFilterSetting
{
    /**
     * @var Registry
     */
    public $registry;

    /**
     * FixAmastyFilterSetting constructor.
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Around plugin to avoid error due to missing constant AMSHOPBYSEO_FOLLOW
     *
     * @param FilterSetting $subject
     * @param callable $proceed
     *
     * @return bool
     */
    public function aroundIsAddNofollow(FilterSetting $subject, callable $proceed): bool
    {
        if (defined('\Amasty\ShopbySeo\Helper\Meta::AMSHOPBYSEO_FOLLOW')) {
            return $proceed();
        } else {
            if (!$subject->getShopbySeoHelper()->isEnableRelNofollow()
                || !$subject->getRelNofollow()
            ) {
                $result =  false;
            } else {
                $result = true;
            }

            return $result;
        }
    }
}
