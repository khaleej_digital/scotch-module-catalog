<?php
declare(strict_types=1);

namespace Beside\Catalog\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;

/**
 * Catalog data helper for media images
 */
class Data extends AbstractHelper
{
    /**
     * @var GalleryReadHandler
     */
    protected $galleryReadHandler;

    /**
     * Catalog Image Helper
     *
     * @var Image
     */
    protected $imageHelper;

    /**
     * @param GalleryReadHandler $galleryReadHandler
     * @param Context $context
     * @param Image $imageHelper
     */
    public function __construct(
        GalleryReadHandler $galleryReadHandler,
        Context $context,
        Image $imageHelper
    ) {
        $this->imageHelper = $imageHelper;
        $this->galleryReadHandler = $galleryReadHandler;
        parent::__construct($context);
    }

    /**
     * Add image gallery
     *
     * @return void
     * @param  ProductInterface $product
     */
    public function addGallery(ProductInterface $product): void
    {
        $this->galleryReadHandler->execute($product);
    }

    /**
     * Get product gallery images
     *
     * @param ProductInterface
     *
     * @return  array|DataObject
     */
    public function getGalleryImages(ProductInterface $product)
    {
        $images = $product->getMediaGalleryImages();
        if ($images instanceof \Magento\Framework\Data\Collection) {
            foreach ($images as $image) {
                /** @var $image \Magento\Catalog\Model\Product\Image */
                $image->setData(
                    'small_image_url',
                    $this->imageHelper->init($product, 'product_page_image_small')
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'medium_image_url',
                    $this->imageHelper->init($product, 'product_page_image_medium')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'large_image_url',
                    $this->imageHelper->init($product, 'product_page_image_large')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
            }
        }

        return $images;
    }
}
