<?php
declare(strict_types=1);

namespace Beside\Catalog\Console\Command;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Beside\Catalog\Model\Import\ProductAttribute;
use Zend_Validate_Exception;

/**
 * Class ImportProductAttribute
 *
 * @package Beside\Catalog\Console\Command
 */
class ImportProductAttribute extends ConsoleCommand
{
    /** @var string Command name for attribute import */
    const COMMAND_NAME = 'beside:product-attributes:import';

    /** @var string Command name label for attribute import */
    const COMMAND_LABEL = 'Import product attributes. var/import/product';

    /**
     * @var EavSetupFactory
     */
    private EavSetupFactory $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private ModuleDataSetupInterface $moduleDataSetup;

    /**
     * @var ProductAttribute
     */
    private ProductAttribute $productAttribute;

    /**
     * ImportProductAttribute constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ProductAttribute $productAttribute
     * @param string|null $name
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup,
        ProductAttribute $productAttribute,
        string $name = null
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->productAttribute = $productAttribute;
        parent::__construct($name);
    }

    /**
     * Configure the parameters.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(self::COMMAND_LABEL);
    }

    /**
     * Import product attributes
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('import product attributes: START');
        $this->productAttribute->importProductAttributes();
        $output->writeln('import product attributes: END');
    }
}
