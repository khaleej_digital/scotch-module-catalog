<?php
declare(strict_types=1);

namespace Beside\Catalog\Console\Command;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Command\Command;
use Beside\Catalog\Model\Import\Category as ModelImportCategory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory;

/**
 * Class ImportCategory
 *
 * @package Beside\Catalog\Console\Command
 */
class ImportCategory extends Command
{
    /** @var string Command name for categories import */
    const COMMAND_NAME = 'beside:category:import';

    /** @var string Command name label for categories import */
    const COMMAND_LABEL = 'Import categories. var/import/category';

    /**
     * @var ModelImportCategory
     */
    private ModelImportCategory $importCategory;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;

    /**
     * ImportCatalog constructor.
     *
     * @param ModelImportCategory $importCategory
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryFactory $categoryFactory
     * @param string|null $name
     */
    public function __construct(
        ModelImportCategory $importCategory,
        StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        CategoryFactory $categoryFactory,
        string $name = null
    ) {
        $this->importCategory = $importCategory;
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->categoryFactory = $categoryFactory;

        parent::__construct($name);
    }

    /**
     * Configure the parameters.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(self::COMMAND_LABEL);
    }

    /**
     * Import categories
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     * @throws FileSystemException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('import categories: START');
        $this->importCategory->importCategories();
        $output->writeln('import categories: END');
    }
}
