<?php
/**
 * @category  Beside
 * @package   Beside_Catalog
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

/**
 * Why we are replacing Amasty\SeoToolKit\Model\Source\Eav\Robots with Beside\Catalog\Model\Source\Eav\Robots?
 * The issue was about virtual class in Amasty_SeoToolkit module.
 * Magento OOTB export functionality is rendering attribute source options in below class
 * \Magento\Backend\Block\Widget\Grid\Column
 * $renderedValue = call_user_func($frameCallback, $renderedValue, $row, $this, false);
 * This source model should be physically exist otherwise export functionality doesn't work.
 *
 * Class UpdateAmtoolkitRobotsAttribute
 *
 * @package Beside\Catalog\Setup\Patch\Data
 */
class UpdateAmtoolkitRobotsAttribute implements DataPatchInterface, PatchRevertableInterface
{
    private $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Add Address Validations
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            \Amasty\SeoToolKit\Model\RegistryConstants::AMTOOLKIT_ROBOTS,
            'source_model',
            'Beside\Catalog\Model\Source\Eav\Robots'
        );
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            \Amasty\SeoToolKit\Model\RegistryConstants::AMTOOLKIT_ROBOTS,
            'source_model',
            'Amasty\SeoToolKit\Model\Source\Eav\Robots'
        );
    }
}
